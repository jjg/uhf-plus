/*

LCD 165x103x6mm
LCD edge: 10mm on three sides except the one with the connectors,
on that side there's 20mm on top and 20mm from the bottom to mount against
4:3 centered image 125x86mm

Channel selector
45mm max
10mm mounting hole
6mm shaft
9mm mounting shaft length
11mm shaft length
24mm height clearance behind mounting panel

*/
include </home/jason/Development/models/components/knob.scad>

VERSION = "Version 0.2";

module lcd(){
    l = 166; //165;
    w = 104; //103;
    h = 7; //6;
    
    cube([l,w,h]);
    
    // blocks for connectors
    translate([l-19,20,-15]){
        cube([20,w-40,10]);
        // TODO: reduce this once we know how much room the wiring really needs
        //cube([40,w-40,40]);
    }
    
    // 4:3 visible area
    translate([(l/2)-(125/2),(w/2)-(86/2),6]){
        cube([125,86,5]);
    }
}

module selector(){
    cylinder(r=45/2,h=24);
    translate([0,0,24]){
        cylinder(r=10/2,h=9);
        translate([0,0,9]){
            cylinder(r=6/2,h=11);
        }
    }
}

module front_panel(){

    l = 200;
    w = 200;
    h = 10;
    
    difference(){
        
        union(){
            color("tan")
            cube([l,w,h]);
            
            // frames around screen, controls
            color("brown")            
            translate([0,0,h]){
                cube([10,w,5]);
                translate([l-10,0,0]){
                    cube([10,w,5]);
                }
                cube([l,10,5]);
                translate([0,w/4,0]){
                    cube([l,10,5]);
                }
                translate([0,w-10,0]){
                    cube([l,10,5]);
                }
            }
        }
        
        // name placard
        /*
        translate([(l/2)-28,(w/4)-20,8]){
            // TODO: put a border around this
            linear_extrude(5){
                // TODO: pick a better font
                text("UHF+ Type 2",size=7);
            }
            translate([7,-10,0]){
                linear_extrude(5){
                    text(VERSION,size=6);
                }
            }
        }
        */

        // TODO: dish-out around screen
        sl = 125;
        sw = 86;
        translate([(l/2)-(sl/2),70,0]){
            translate([0,11,-1]){
                cube([sl,sw,12]);
            }
        }
        
        // make room for minerals
        translate([(l/2)-(165/2),(w*.5)-28,-6]){
            lcd();
        }
        translate([l/4,(l/4)-(40/2),-18]){
            selector();
            translate([0,0,35]){
                knob(diameter=20,skirt=35,knurls=0,position=-1);
            }
        }
        translate([l-(l/4),(l/4)-(40/2),-18]){
            selector();
            translate([0,0,35]){
                knob(diameter=20,skirt=35,knurls=0,position=0,scale_start=0,scale_end=11);
            }
        }
    }
}

// back cabinet
module back(){
    l = 200;
    w = 200;
    h = 190;
    
    // TODO: add foot
    // TODO: add vents
    // TODO: add electronics mounts
    // TODO: add usb, power openings
    
    difference(){
        union(){
            hull(){
                translate([15,10,0]){
                    cube([l-30,w-30,10]);
                }
                translate([0,0,180]){
                    cube([l,w,10]);
                }
            }
            
            // foot
            translate([(l*.5)-((l-(l*.25))/2),-10,0]){
                cube([l-(l*.25),20,190]);
            }
        }
        
        // LCD mount
        translate([(l/2)-(165/2)-0.5,(w*.5)-28+0.5,h-6-0.5]){
            //scale([1.01,1,1]){
                lcd();
            //}
        }
        
        // hollow-out behind LCD
        translate([(l/2)-(165/2)+10,(w/2)-28+10,5]){
            cube([150,85,h]);
        }
        
        // hollow-out behind controls
        translate([(l/2)-(165/2),5,5]){
            cube([165,65,h]);
        }
        
        // channel between controls and lcd
        translate([(l*.5)-((l*.25)/2),65,45]){
            cube([l*.25,20,150]);
        }
            
        // handle
        translate([w/2-((w*.75/2)),l-10,90]){
            rotate([18,0,0]){
                cube([w*.75,25,80]);
            }
        }
        
        // front panel screw holes
        translate([7,7,0]){
            cylinder(r=10,h=180);
            cylinder(r=3,h=200);
            translate([0,l-14,0]){
                cylinder(r=10,h=180);
                cylinder(r=3,h=200);
            }
        }
        translate([l-7,7,0]){
            cylinder(r=10,h=180);
            cylinder(r=3,h=200);
            translate([0,l-14,0]){
                cylinder(r=10,h=180);
                cylinder(r=3,h=200);
            }
        }
        
        // upper cooling vents
        slots = 8;
        translate([8,0,3]){
            for(i=[1:slots]){
                translate([20*i,160,0]){
                    cube([3,35,h*.78]);
                }
            }
        }
        
        // TODO: refine these rear panel openings once part placement is set
        translate([30,82,-1]){
            cube([l*.73,15,10]);
        }
        
        // testing split
        translate([0,-15,-18]){
            //cube([l+10,w+20,h+10]);
        }
    }
}


// assemble!
l = 200;
w = 200;
h = 10;

front_panel();

translate([0,0,-190]){
    color("tan")
    back();
}


translate([(l/2)-(165/2),(w*.5)-28,-6]){
    color("silver")
    lcd();
}


translate([l/4,(l/4)-(40/2),-18]){
    selector();
    translate([0,0,35]){
        color("gold")
        knob(diameter=20,skirt=35,knurls=0,position=-1);
    }
}
translate([l-(l/4),(l/4)-(40/2),-18]){
    selector();
    translate([0,0,35]){
        color("gold")
        knob(diameter=20,skirt=35,knurls=0,position=0,scale_start=0,scale_end=11);
    }
}
